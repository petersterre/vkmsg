var vkchat = angular.module('vkchat.decorator', []);
vkchat.service('decorator', function($q) {
    this.decorate = function(message) {
        var deferred = $q.defer();
        setTimeout(function() {
            if(Math.abs(Math.floor(Math.random() * 100)) % 5 == 0) {
                deferred.reject('error');
            } else {
                message.body = '+++' + message.body + '+++';
                deferred.resolve(message);
            }
        }, 500);
        return deferred.promise;
    }; 
});
