'use strict';

var vkchat = angular.module('vkchat', [
    'ngRoute',
    'vkchat.controllers',
    'vk',
    'vkchat.decorator'
]);

vkchat.config(function($routeProvider) {
    $routeProvider
    .when('/chat/:uid', {
        templateUrl: 'templates/main.html',
        controller: 'MainCtrl'
    });
});


