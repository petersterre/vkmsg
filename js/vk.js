angular.module('vk', []).
    factory('vk', function($http, $q) {
        return function(method, params) {
            var deferred = $q.defer();
            params = angular.extend(params, {
                access_token: localStorage.getItem('access_token'),
                callback: 'JSON_CALLBACK'
            });
            $http({
                url:'https://api.vk.com/method/' + method, 
                method: 'JSONP',
                params: params
            })
                .success(function(data) {
                    if(data.error) {
                        deferred.reject(data.error);
                    } else if(data.response) {
                        deferred.resolve(data.response);
                    } else {
                        deferred.reject();
                    }
                    
                })
                .error(function(data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        
        };
    });
